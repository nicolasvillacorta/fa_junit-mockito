package com.accenture.ejemplo.model;

public class Persona {

	private int dni;
	private String nombre;
	private int edad;
	private int sueldo;
	public int getDni() {
		return dni;
	}
	public void setDni(int dni) {
		this.dni = dni;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public int getSueldo() {
		return sueldo;
	}
	public void setSueldo(int sueldo) {
		this.sueldo = sueldo;
	}
	
	public boolean isMayor18() {
		if(this.edad>=18) {
			return true;
		}else {
			return false;
		}
	}
	
	public boolean isSalarioAlto() {
		if(this.sueldo>=5000) {
			return true;
		}else {
			return false;
		}
	}
}
