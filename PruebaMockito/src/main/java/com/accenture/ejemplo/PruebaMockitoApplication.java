package com.accenture.ejemplo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaMockitoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaMockitoApplication.class, args);
	}

}
