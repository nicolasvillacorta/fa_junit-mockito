package com.accenture.ejemplo.model;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

public class PrestamoTest {

	private Persona personaValida;
	private Persona	personaInvalida;
	private Prestamo prestamo;
	
	@Before
	public void setUp() {
		prestamo = new Prestamo();
		// Uso un mock
		personaValida = mock(Persona.class);
		personaInvalida = mock(Persona.class);
		when(personaInvalida.isMayor18()).thenReturn(false);
		when(personaInvalida.isSalarioAlto()).thenReturn(false);
		when(personaValida.isMayor18()).thenReturn(true);
		when(personaValida.isSalarioAlto()).thenReturn(true);
		
		prestamo.setCantCuotas(50);
		prestamo.setCapital(100000);
		prestamo.setTasaInteres(30);
		prestamo.setId(1L);
		
		
	}
	
	// El test se pasa si recibe la exception expuesta.
	@Test(expected = IllegalArgumentException.class)
	public void testSetPersonaInvalida() {
		prestamo.setPersona(personaInvalida);
		
	}
	
	@Test
	public void testSetPersonaValida() {
		prestamo.setPersona(personaValida);
		// Verifica que personaValida haya hecho la llamada a estos metodos.
		verify(personaValida).isMayor18();
		verify(personaValida).isSalarioAlto();
		
	}

}
