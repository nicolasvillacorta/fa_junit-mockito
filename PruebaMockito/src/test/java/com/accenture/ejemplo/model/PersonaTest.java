package com.accenture.ejemplo.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


// No tiene mocks.
public class PersonaTest {
	
	private Persona p;

	
	@Before
	public void setUp() {
		p = new Persona();
		p.setDni(38890422);
		p.setEdad(18);
		p.setNombre("Nicolas");
		p.setSueldo(25000);
	}

	
	@Test
	public void testGestDni() {
		int resultado = p.getDni();
		int resultadoEsperado = 38890422;
		
		Assert.assertEquals(resultadoEsperado, resultado);
	}
	
	@Test
	public void testSetDni() {
		int dni = 22222222;
		p.setDni(dni);
		
		Assert.assertNotNull(p.getDni());
	}
	
	@Test
	public void testIsMayor18() {
		Assert.assertTrue(p.isMayor18());
	}
	
	

}
